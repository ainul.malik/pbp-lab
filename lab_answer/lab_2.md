1. What's the difference between JSON and XML?

JSON is a file format that stands for JavaScript Object Notation. It is based on the JavaScript programming language and its main use is to transfer data in between servers and web applications that are also based on JavaScript.

XML is also a file format and it stands for Extensible Markup Language. Just like JSON, its main use is for transferring data to and from web servers.

While they both have the same general use, the difference between JSON and XML mainly lies in the way the data is stored. Both formats are human readable and can be processed using various programming languages, but differs in that JSON is easier to process than XML and in notational differences like tags being used in XML while JSON doesn't, and that JSON supports arrays.

2. What's the difference between HTML and XML

While HTML and XML are both markup languages, HTML is used for the presenting of data, whereas XML is more concerned with the transfer of data.