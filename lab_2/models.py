from django.db import models

# Create your models here.

class Note(models.Model):
    to = models.CharField(max_length=255)
    frm = models.CharField(max_length=255)
    title = models.CharField(max_length=255)
    message = models.TextField()